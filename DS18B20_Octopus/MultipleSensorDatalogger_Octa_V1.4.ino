// Include the libraries we need
#include <OneWire.h>
#include <DallasTemperature.h>
// Data wire is plugged into port 2 on the Arduino
#define ONE_WIRE_BUS 2
#define TEMPERATURE_PRECISION 12

// Low Power Lib
#include "LowPower.h"

// For SD
//#include <SPI.h>
#include <SdFat.h>
SdFat sd;
SdFile myFile;
const int chipSelect = 10;
String dataString;

// DS232 library and variables
#include <DS3232RTC.h>    //http://github.com/JChristensen/DS3232RTC
#include <TimeLib.h>         //http://www.arduino.cc/playground/Code/Time  
#include <Wire.h>         //http://arduino.cc/en/Reference/Wire (included with Arduino IDE)


// Compleeting setup there is an IRF 9540 connected to pin 3 powering sensors, a Led on Pin 4 and a Switch on Pin 5
#define POWER_PIN 3  // power on pin - if set to LOW we will provide energy to the RTC, to the SD and to the DS Sernsors
#define LED_PIN 4   // Led for communication
#define SW_PIN 5  // Switch button pin


// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature.
DallasTemperature sensors(&oneWire);

// arrays to hold device addresses
DeviceAddress Sonda1, Sonda2, Sonda3, Sonda4;
DeviceAddress Sonda5, Sonda6, Sonda7, Sonda8;


void setup(void)
{
  // start serial port
  Serial.begin(9600);

  //Power on Sensors PIN
  pinMode(POWER_PIN, OUTPUT);
  pinMode(LED_PIN, OUTPUT);
  pinMode(SW_PIN, INPUT);
  digitalWrite(SW_PIN, HIGH); //turning on SW oullup resistor

  //Turning ON all devices to setting Up

  digitalWrite(POWER_PIN, LOW);
  digitalWrite(LED_PIN, HIGH);

  delay(100);

  Serial.println("V8.1.3 - Do you want to read data?");
  Serial.println("Tipe any char to read data.");
  countdown(1);

  Serial.println();
  //// the function to get the time from the RTC
  setTime(RTC.get());
  if (timeStatus() != timeSet)
    Serial.println("Unable to sync with the RTC");
  else
    Serial.println("RTC has set the system time");

  //  if (!sd.begin(chipSelect, SPI_HALF_SPEED)) sd.initErrorHalt();


  // Start up the library
  sensors.begin();

  // assigns the first address found to insideThermometer
  if (!oneWire.search(Sonda5)) Serial.println("Unable to find address for Sonda5");//1
  if (!oneWire.search(Sonda1)) Serial.println("Unable to find address for Sonda1");//2
  if (!oneWire.search(Sonda6)) Serial.println("Unable to find address for Sonda6");//3
  if (!oneWire.search(Sonda2)) Serial.println("Unable to find address for Sonda2");//4
  if (!oneWire.search(Sonda3)) Serial.println("Unable to find address for Sonda3");//5
  if (!oneWire.search(Sonda4)) Serial.println("Unable to find address for Sonda4");//6
  if (!oneWire.search(Sonda7)) Serial.println("Unable to find address for Sonda7");//7
  if (!oneWire.search(Sonda8)) Serial.println("Unable to find address for Sonda8");//8



// //  show the addresses we found on the bus
//      Serial.print("Device 0 Address: ");
//      printAddress(Sonda1);
//      Serial.println();
//  
//      Serial.print("Device 1 Address: ");
//      printAddress(Sonda2);
//      Serial.println();
//  
//      Serial.print("Device 2 Address: ");
//      printAddress(Sonda3);
//      Serial.println();
//  
//        Serial.print("Device 3 Address: ");
//      printAddress(Sonda4);
//      Serial.println();
//      
//    Serial.print("Device 4 Address: ");
//      printAddress(Sonda5);
//      Serial.println();
//  
//      Serial.print("Device 5 Address: ");
//      printAddress(Sonda6);
//      Serial.println();
//  
//      Serial.print("Device 6 Address: ");
//      printAddress(Sonda7);
//      Serial.println();
//  
//        Serial.print("Device 7 Address: ");
//      printAddress(Sonda8);
//      Serial.println();

  // set the resolution to 9 bit per device  //5-1-6-2-3-4-7-8
  sensors.setResolution(Sonda5, TEMPERATURE_PRECISION);
  sensors.setResolution(Sonda4, TEMPERATURE_PRECISION);
  sensors.setResolution(Sonda6, TEMPERATURE_PRECISION);
  sensors.setResolution(Sonda2, TEMPERATURE_PRECISION);
  sensors.setResolution(Sonda3, TEMPERATURE_PRECISION);
  sensors.setResolution(Sonda4, TEMPERATURE_PRECISION);
  sensors.setResolution(Sonda7, TEMPERATURE_PRECISION);
  sensors.setResolution(Sonda8, TEMPERATURE_PRECISION);
}

// //function to print a device address
//void printAddress(DeviceAddress deviceAddress)
//{
//  for (uint8_t i = 0; i < 8; i++)
//  {
//    // zero pad the address if necessary
//    if (deviceAddress[i] < 16) Serial.print("0");
//    Serial.print(deviceAddress[i], HEX);
//  }
//}

// function to print the temperature for a device
void printTemperature(DeviceAddress deviceAddress)
{
  float tempC = sensors.getTempC(deviceAddress);
  dataString += tempC ;
  //Serial.print(tempC);
}

// function to print a device's resolution
//void printResolution(DeviceAddress deviceAddress)
//{
//  Serial.print("Resolution: ");
//  Serial.print(sensors.getResolution(deviceAddress));
//  Serial.println();
//}


// main function to print information about a device
void printData(DeviceAddress deviceAddress)
{
  // Serial.print("Device Address: ");
  // printAddress(deviceAddress);
  //  Serial.print("\t");
  printTemperature(deviceAddress);

}

/*
   Main function, calls the temperatures in a loop.
*/


void loop(void)
{
  //  Serial.print("Start Cycle ");
  //  Serial.println(millis());

  digitalWrite(POWER_PIN, LOW);
  digitalWrite(LED_PIN, HIGH);
  delay(10);

  setTime(RTC.get());

  dataString = ""; //Clear previous string

  dataString += day();
  dataString += "/";
  dataString += month();
  dataString += "/";
  dataString += year();
  dataString += " ";
  dataString += hour();
  dataString += ":";
  dataString += minute();
  dataString += ":";
  dataString += second();
  dataString += ",,";

  // call sensors.requestTemperatures() to issue a global temperature
  // request to all devices on the bus
  //Serial.print("Requesting temperatures...");
  sensors.requestTemperatures();
  //Serial.println("DONE");

  // print the device information
  printData(Sonda1);
  dataString += ",";
  printData(Sonda2);
  dataString += ",";
  printData(Sonda3);
  dataString += ",";
  printData(Sonda4);
  dataString += ",";
  printData(Sonda5);
  dataString += ",";
  printData(Sonda6);
  dataString += ",";
  printData(Sonda7);
  dataString += ",";
  printData(Sonda8);


  //Serial.println();
  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  if (!sd.begin(chipSelect, SPI_HALF_SPEED)) Serial.println("Uncapable to begin SD");
  delay(1);
  // open the file for write at end like the Native SD library
  if (!myFile.open("datalog.txt", O_CREAT | O_WRITE | O_APPEND)) Serial.println("Uncapable to open File");

  // if the file is available, write to it:
  // if the file opened okay, write to it:
  //  Serial.print("Writing to test.txt...");
  myFile.print(dataString);
  myFile.println();
  // close the file:
  myFile.sync();
  myFile.close();
  //  Serial.println("done.");
  //Serial.println(dataString);
  // print to the serial port too:
  int cycle;
  //I primi dieci minuti dall'accensione  esegui una scansione ogni 16 secondi, poi ogni
  if (millis() < 1200000) {   //after 24 hours of 10 seconds relevation - 15 relavation circa
    cycle = 2;
    Serial.println(dataString);
  }
  else {
    cycle = 106;  //cicli da 8 secondi.
      }

  //Serial.print("Going sleep ");
  // Serial.println(millis());

  delay(1000);

  digitalWrite(POWER_PIN, HIGH);
  digitalWrite(LED_PIN, LOW);

  // if the file isn't open, pop up an error:
  //  else {
  //  Serial.println("error opening datalog.txt");
  //  }

  for (int counter = 1; counter <= cycle; counter++) {
    LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
    //  LowPower.idle(SLEEP_8S, ADC_OFF, TIMER2_OFF, TIMER1_OFF, TIMER0_OFF,
    //       SPI_OFF, USART0_OFF, TWI_OFF);
  }
  // Serial.print("Wake up ");
  //Serial.println(millis());
}

void countdown(int selection) {
  for (int s = 5; s >= 0; s--) {
    // re-open the file for reading:
    Serial.print(s);
    Serial.print(" ");
    delay(1000);
    if (Serial.available() > 0) {
      Serial.println();
      s = -1;
      //      switch (selection)
      //      {
      //        case 1:

      if (!sd.begin(chipSelect, SPI_HALF_SPEED)) Serial.println("Uncapable to begin SD");
      delay(1);
      if (!myFile.open("datalog.txt", O_READ)) {
        sd.errorHalt("opening datalog_2.txt for read failed");
      }
      Serial.println("----------START OF FILE-----------");
      // read from the file until there's nothing else in it:
      int data;
      while ((data = myFile.read()) >= 0) Serial.write(data);
      Serial.println("----------END OF FILE-----------");
      // close the file:
      myFile.close();


      //          break;
      //        case 2:
      //       // frequency();
      //          break;
      //      }
    }
  }

}

//void frequency(){
//  Serial.println("Inserisci il numero approssimativo di rilevazioni da compiere in un ora");
////I write this numer in the frequency.txt file
//
//if (!sd.begin(chipSelect, SPI_HALF_SPEED)) Serial.println("Uncapable to begin SD");
//          delay(1);
//    sd.remove("frequency.txt");
//
//     if (!myFile.open("frequency.txt", O_CREAT | O_WRITE )) Serial.println("Uncapable to open Frequency File");
//
//
//
//          myFile.close();
//
//
//}

