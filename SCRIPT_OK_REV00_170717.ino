// Example testing sketch for various DHT humidity/temperature sensors
// Written by ladyada, public domain

#include "DHT.h"
#include <Wire.h>
#include "Sodaq_DS3231.h"
#include <SPI.h>
#include <SD.h>
// #include <Sleep_n0m1.h>
//#include "LowPower.h"

#define DHTPIN 9     // what digital pin we're connected to

#define POWER_PIN 6 
#define LIGHT_PIN A1 
// Uncomment whatever type you're using!
#define DHTTYPE DHT11   // DHT 11
//#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

// Connect pin 1 (on the left) of the sensor to +5V
// NOTE: If using a board with 3.3V logic like an Arduino Due connect pin 1
// to 3.3V instead of 5V!
// Connect pin 2 of the sensor to whatever your DHTPIN is
// Connect pin 4 (on the right) of the sensor to GROUND
// Connect a 10K resistor from pin 2 (data) to pin 1 (power) of the sensor

// Initialize DHT sensor.
// Note that older versions of this library took an optional third parameter to
// tweak the timings for faster processors.  This parameter is no longer needed
// as the current DHT reading algorithm adjusts itself to work on faster procs.
DHT dht(DHTPIN, DHTTYPE);

File myFile;

String dataString;

int count = 0;
// Sleep sleep;
unsigned long sleepTime; //how long you want the arduino to sleep

void setup() {

  pinMode(POWER_PIN, OUTPUT);

  digitalWrite (POWER_PIN, LOW);

  delay (500);

  Serial.begin(9600);
  // Serial.println("DHTxx test!");
  
  rtc.begin();

  dht.begin();

  if (!SD.begin(10)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");

}

void loop() {
  digitalWrite(POWER_PIN, LOW);   // turn the LED on (HIGH is the voltage level)

  // Wait a few seconds between measurements.
  delay(200);

  int light=analogRead(LIGHT_PIN);

  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t)) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }

  DateTime now = rtc.now(); //get the current date-time
    Serial.print(now.year(), DEC);
    Serial.print('/');
    Serial.print(now.month(), DEC);
    Serial.print('/');
    Serial.print(now.date(), DEC);
    Serial.print(' ');
    Serial.print(now.hour(), DEC);
    Serial.print(':');
    Serial.print(now.minute(), DEC);
    Serial.print(':');
    Serial.print(now.second(), DEC);
    Serial.print('-->');
    Serial.print("Humidity: ");
    Serial.print(h);
    Serial.print(" %");
    Serial.print("Temperature: ");
    Serial.print(t);
    Serial.print(" *C ");
    Serial.print("Light: ");
    Serial.print(light);
    Serial.print("/1023\t");
// 
  dataString = "";
  dataString += now.year();
  dataString += "/";
  dataString += now.month();
  dataString += "/";
  dataString += now.date();
  dataString += ",";
  dataString += now.hour();
  dataString += ":";
  dataString += now.minute();
  dataString += ":";
  dataString += now.second();
  dataString += ",";
  dataString += h;
  dataString += "%,";
  dataString += t;
  dataString += ",";
  dataString += light;

  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  myFile = SD.open("test.txt", FILE_WRITE);

  // if the file opened okay, write to it:
  if (myFile) {
    Serial.print("Writing to test.txt...");
    myFile.println(dataString);
    // close the file:
    myFile.close();
    Serial.println("done.");
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }

  delay (500);

  // Spegnimento totale di tutto!!!
  digitalWrite(POWER_PIN, HIGH);    // turn the LED off by making the voltage LOW
  //Serial.print("sleeping ");
  delay(10000); //delay to allow serial to fully print before sleep
  // Enter power down state for 8 s with ADC and BOD module disabled
 // for (int i=0 ; i<=110 ; i++ )
// {LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);}  
 // Serial.print("wakeup");
}
